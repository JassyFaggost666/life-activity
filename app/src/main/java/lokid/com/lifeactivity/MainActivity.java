package lokid.com.lifeactivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;


/**
 *
 */
public class MainActivity extends AppCompatActivity {

    /**
     *  Tag Log
     */
    private static final String TAG = "MainActivity";

    /**
     *  Колличество студентов
     */
    private Integer counterStudents = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }



    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        toast("onStart");
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        toast("onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        toast("onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        toast("onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
        toast("onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        toast("onDestroy");
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count", counterStudents);
        Log.d(TAG, "onSaveInstanceState");
        toast("onSaveInstanceState");
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null &&
                savedInstanceState.containsKey("count")) {
            counterStudents = savedInstanceState.getInt("count");
            outputCountOfStudents();
        }
        Log.d(TAG, "onRestoreInstanceState");
        toast("onRestoreInstanceState");
    }


    /**
     * Обновление на активити, счётчик студентов
     */
    public void outputCountOfStudents() {
        TextView counterView = (TextView) findViewById(R.id.text_counter);
        counterView.setText(String.format(Locale.getDefault(), "%d", counterStudents));
    }


    /**
     * Увеличение счётчика студентов на 1
     * @param view - View в которой исполняется событие
     */
    public void onClickButtonAddStudents(View view) {
        counterStudents++;
        toast("+1");
        outputCountOfStudents();
    }


    /**
     * Вывод Строки на эран в виде {@link Toast}
     * @param state - строка
     */
    public void toast(String state) {
        Toast.makeText(getApplicationContext(),
                state, Toast.LENGTH_SHORT).show();
    }

}
